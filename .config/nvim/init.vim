" Plugins
call plug#begin('~/.config/nvim/bundle')

Plug 'honza/vim-snippets' " snippets repo
Plug 'garbas/vim-snipmate' " snippets manager
Plug 'MarcWeber/vim-addon-mw-utils' " dependencies #1
Plug 'tomtom/tlib_vim' " dependencies #2
Plug 'tpope/vim-surround' " brackets and more
Plug 'jiangmiao/auto-pairs' " auti brackets
Plug 'preservim/nerdcommenter' " comment functions
Plug 'lilydjwg/colorizer' " show colors
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'whatyouhide/vim-gotham' " colorscheme
Plug 'numirias/semshi', {'do': ':UpdateRemotePlugins'} "python highlighting
Plug 'dense-analysis/ale' " syntax check
Plug 'davidhalter/jedi-vim' " autocompletion
Plug 'terryma/vim-multiple-cursors' " multicursor
call plug#end()

" Theme
syntax enable " enables syntax highlighing
set termguicolors
colorscheme gotham

" Tab and Indent configuration
set expandtab " use spaces instead of tabs
set tabstop=4 " one tab == four spaces
set shiftwidth=4 " one tab == four spaces
set smarttab " be smart using tabs ;) (c)

" Turn off backup
set nobackup " no auto backups
set noswapfile " no swap
set nowritebackup " no backups

" Other options
set number " line numbers
set mouse=a " enable mouse
set laststatus=2 " always show statusline
set incsearch " incremental search
set hidden " keep multiple buffers open
"set nowrap " display long lines as just one line
set encoding=utf-8 " the encoding displayed 
set splitbelow " below horizontal splits
set splitright " right vertical splits
set clipboard=unnamedplus " fix clipboard

" Cursor line
set cursorline
set cursorcolumn

" Ale
let g:ale_lint_on_enter = 0
let g:ale_lint_on_text_changed = 'never'
let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_linters = {'python': ['flake8']}

" Airline
let g:airline_theme='gotham'
let g:airline_left_sep  = ''
let g:airline_right_sep = ''
let g:airline#extensions#ale#enabled = 1
let airline#extensions#ale#error_symbol = 'E:'
let airline#extensions#ale#warning_symbol = 'W:'

" SnipMate
let g:snipMate = { 'snippet_version' : 1 }
