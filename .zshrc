# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
# TERM="xterm-256color"
# Path to your oh-my-zsh installation.
  export ZSH=/home/oleg/.oh-my-zsh

DEFAULT_USER="oleg"

# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

autoload -U compinit
compinit -d ~/.cache/zsh/zcompdump-$ZSH_VERSION

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="typewritten"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git archlinux zsh-syntax-highlighting web-search zsh-autosuggestions vundle pass colored-man-pages)

source $ZSH/oh-my-zsh.sh
source .oh-my-zsh/custom/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#5C6773"
# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
export EDITOR='nvim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# # Quick Switch To Directories
alias down='cd ~/Downloads'
alias code='cd ~/Code'

# Quick Config Editing
alias vimrc='vim ~/.vimrc'
alias bspwmrc='vim ~/.config/bspwm/bspwmrc'
alias sxhkdrc='vim ~/.config/sxhkd/sxhkdrc'
alias xresources='vim ~/.Xresources'
alias polybarconf='vim ~/.config/polybar/config'
alias htop='htop -u mohabaks'

# Miscellaneous Commands
alias htop='htop -u oleg'
alias fm='ranger'
alias l='exa -lar'
alias ..='cd ..'
alias df='df -h'
alias pacman='sudo pacman'
alias t='gio list trash://'
alias tt='gio trash --empty'
alias c='clear'
alias n='ncmpcpp'
alias v='nvim'
alias vim='nvim'
alias g='git'
alias gs='git status'
alias hdd='lsblk -a'
alias hddu='umount /dev/sda2'
alias archwiki='awman'
alias calc='qalc'
alias vpn='sudo protonvpn c -f'
alias vpnd='sudo protonvpn disconnect'
alias vpns='protonvpn status'
alias py='python'
alias cat='bat'
alias wifi='nmtui'
alias iphone='ifuse /run/media/iphone/'

# Позволяет не писать cd перед адресом
setopt autocd


